import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';

import { CategoryInterface } from './interface/category.interface';
import { Category } from './models/category.model';

@Injectable()
export class CategoryService {
  constructor(@InjectModel('Category') private readonly categoryModel: Model<CategoryInterface>) {}

  async findAll(): Promise<Category[]> {
    return await this.categoryModel.find().exec() as Category[];
  }
}
