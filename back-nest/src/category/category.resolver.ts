import { Resolver, Query } from '@nestjs/graphql';

import { CategoryService } from './category.service';
import { Category } from './models/category.model';

@Resolver('Category')
export class CategoryResolver {
  constructor(private readonly service: CategoryService) {}

  @Query(() => [Category])
  async categories(): Promise<Category[]> {
    return await this.service.findAll();
  }
}
