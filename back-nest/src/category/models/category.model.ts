import { Field, ID, ObjectType } from '@nestjs/graphql';

@ObjectType({ description: 'Categories model' })
export class Category {
  @Field(() => ID)
  _id: string;

  @Field({ description: 'Category title' })
  title: string;
}