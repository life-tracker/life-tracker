import { ID, Field, ObjectType } from '@nestjs/graphql';
import { Document } from 'mongoose';

@ObjectType()
export class CategoryInterface extends Document {
  @Field(() => ID)
  _id: string;

  @Field()
  title: string;  
}
