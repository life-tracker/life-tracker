import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { CategorySchema } from './schemas/category.schema';
import { CategoryResolver } from './category.resolver';
import { CategoryService } from './category.service';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'Category', schema: CategorySchema, collection: 'category' }])],
  providers: [CategoryResolver, CategoryService]
})
export class CategoryModule {}
