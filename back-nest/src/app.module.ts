import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { GraphQLModule } from '@nestjs/graphql';
import { MongooseModule } from '@nestjs/mongoose';

import { TrackerModule } from './tracker/tracker.module';
import { CategoryModule } from './category/category.module';

@Module({
  imports: [
    TrackerModule,
    CategoryModule,
    ConfigModule.forRoot(),
    GraphQLModule.forRoot({
      autoSchemaFile: 'schema.gql',
    }),
    MongooseModule.forRoot(process.env.MONGO_CONNECT_URL, { 
      useNewUrlParser: true, 
      useFindAndModify: false, // https://stackoverflow.com/questions/52572852/deprecationwarning-collection-findandmodify-is-deprecated-use-findoneandupdate
    }),
  ],
})
export class AppModule {}
