import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { TrackerSchema } from './schemas/tracker.schema';
import { TrackerResolver } from './tracker.resolver';
import { TrackerService } from './tracker.service';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'Tracker', schema: TrackerSchema, collection: 'tracker' }])],
  providers: [TrackerResolver, TrackerService],
})
export class TrackerModule {}
