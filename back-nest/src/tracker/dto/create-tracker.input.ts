import { Field, InputType } from '@nestjs/graphql';
import { IsDate, MaxLength, IsString } from 'class-validator';

@InputType()
export class NewTrackerInput {
  @Field({ nullable: true })
  @IsDate()
  date: Date;

  @Field({ nullable: true })
  @IsString()
  @MaxLength(300)
  title: string;

  @Field({ nullable: true })
  @IsString()
  @MaxLength(50)
  category: string;
}
