import { InputType, ObjectType, ArgsType, Field, Int, registerEnumType } from '@nestjs/graphql';
import { Max, Min, IsOptional, IsInt } from 'class-validator';
import { OrderByDirection } from './enums';

registerEnumType(OrderByDirection, {
  name: 'OrderByDirection',
});

@InputType()
export class SortFilter {
  @Field(() => OrderByDirection, { nullable: true })
  @IsOptional()
  date: OrderByDirection;

  @Field(() => OrderByDirection, { nullable: true })
  @IsOptional()
  title?: OrderByDirection;
}

// @ArgsType()
// export class TrackerArgs {
//   @Field(type => [DateArgs])
//   sort: DateArgs[];

//   @Field(type => Int)
//   @Min(1)
//   @Max(50)
//   take = 25;
// }

// @ArgsType()
// export class SortArgs {
//   @Field()
//   sort: DateArgs;
// }

  // @Field({ defaultValue: '' })
