export enum OrderByDirection {
  asc = 1,
  desc = -1,
}

export enum SortableField {
  date,
  title,
}
