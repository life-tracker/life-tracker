import { ID, Field, ObjectType } from '@nestjs/graphql';
import { Document } from 'mongoose';

@ObjectType()
export class TrackerInterface extends Document {
  @Field(() => ID)
  _id: string;

  @Field()
  title: string;

  @Field()
  date: Date;

  @Field()
  category: string;
}
