import { Resolver, Query, Mutation, Args, ID } from '@nestjs/graphql';

import { TrackerService } from './tracker.service';
import { Tracker } from './models/tracker.model';
import { SortFilter } from './dto/tracker.args';
import { NewTrackerInput } from './dto/create-tracker.input';

@Resolver()
export class TrackerResolver {
  constructor(private readonly service: TrackerService) {}
  
  @Query(() => [Tracker])
  async posts(
    @Args({ 
      name: 'sort', 
      nullable: true, 
      type: () => SortFilter, 
      defaultValue: { date: -1 } 
    }) args: SortFilter,
  ): Promise<Tracker[]> {
    return await this.service.findAll(args);
  }

  @Query(() => Tracker)
  async post(@Args('id', { type: () => ID }) id: string) {
    return await this.service.findOne(id);
  }

  @Mutation(() => Boolean)
  async remove(@Args('id', { type: () => ID }) id: string) {
    return await this.service.remove(id);
  }

  @Mutation(() => Tracker)
  async create(@Args('post') post: NewTrackerInput): Promise<Tracker> {
    return await this.service.create(post);
  }

  @Mutation(() => Tracker)
  async update(
    @Args('id', { type: () => ID }) id: string, 
    @Args('post') post: NewTrackerInput,
  ): Promise<Tracker> {
    return await this.service.update(id, post);
  }
}
