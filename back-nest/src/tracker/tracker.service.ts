import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';

import { TrackerInterface } from './interfaces/tracker.interface';
import { Tracker } from './models/tracker.model';
import { NewTrackerInput } from './dto/create-tracker.input';

@Injectable()
export class TrackerService {
  constructor(@InjectModel('Tracker') private readonly trackerModel: Model<TrackerInterface>) {}

  async findAll(sort): Promise<Tracker[]> {
    return await this.trackerModel.find().sort(sort).exec() as Tracker[];
  }

  async findOne(id: string): Promise<Tracker> {
    return await this.trackerModel.findById(id) as Tracker;
  }

  async create(post: NewTrackerInput): Promise<Tracker> {
    const createdTracker = new this.trackerModel(post);
    return createdTracker.save();
  }

  async update(id: string, post: NewTrackerInput) {
    const result = await this.trackerModel.findByIdAndUpdate(id, post, { new: true });
    return result as Tracker;
  }

  async remove(id: string): Promise<boolean> {
    const { n } = await this.trackerModel.deleteOne({ _id: id });
    return !!n as boolean; // number to boolean
  }
}
