import * as mongoose from 'mongoose';

export const TrackerSchema = new mongoose.Schema({
  title: { type: String, required: true },
  date: { type : Date, default: Date.now },
  category: { type: String, required: true },
} ,{
  timestamps: true, // This option adds createdAt and updatedAt properties that are timestamped with a Date, and which does all the work for you. Any time you update the document, it updates the updatedAt property.
  // versionKey: false, // by default using NestJS creating a document in MongoDB, it adds extra column "_v" is versioning to the document
});
