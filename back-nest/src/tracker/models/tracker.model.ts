import { Field, ID, ObjectType } from '@nestjs/graphql';

@ObjectType({ description: 'Trackers model' })
export class Tracker {
  @Field(() => ID)
  _id: string;
  
  @Field({ description: 'Tracker date' })
  date: Date;

  @Field({ description: 'Tracker title' })
  title: string;

  @Field({ description: 'Tracker category' })
  category: string;
}