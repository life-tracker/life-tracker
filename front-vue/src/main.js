import Vue from 'vue';
import Buefy from 'buefy';
import App from './App.vue';

import 'buefy/dist/buefy.css';
import '@mdi/font/css/materialdesignicons.css';
import { createProvider } from './vue-apollo';

Vue.use(Buefy);

Vue.config.productionTip = false;

new Vue({
  apolloProvider: createProvider(),
  render: (h) => h(App),
}).$mount('#app');
