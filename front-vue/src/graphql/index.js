import gql from 'graphql-tag';

const POSTS_ALL = gql`query ($sort: SortFilter) {
  posts(sort: $sort) {
    _id
    date
    title
    category
  }
}`;

const POST_CREATE = gql`mutation ($post: NewTrackerInput!) {
  create(post: $post) {
    _id
    date
    title
    category
  }
}`;

const POST_REMOVE = gql`mutation removeMutation($id: ID!) {
  remove(id: $id)
}`;

const POST_UPDATE = gql`mutation createMutation($id: ID!, $post: NewTrackerInput!) {
  update(id: $id, post: $post) {
    _id
    date
    title
    category
  }
}`;

const ALL_CATEGORIES = gql`query {
  categories {
    _id
    title
  }
}`;

export {
  POSTS_ALL,
  POST_CREATE,
  POST_REMOVE,
  POST_UPDATE,
  ALL_CATEGORIES,
};
