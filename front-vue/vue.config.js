module.exports = {
  devServer: {
    proxy: 'http://localhost:4000/',
  },
  pluginOptions: {
    apollo: {
      // Enable Apollo Engine
      enableEngine: false,
      // Enable ESLint for `.gql` files
      lintGQL: true,
    },
  },
};
