module.exports = {
  root: true,

  env: {
    browser: true,
    node: true,
  },

  extends: [
    'airbnb-base',
    'plugin:vue/recommended',
    'plugin:vue/essential',
    '@vue/airbnb',
  ],

  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'vue/html-indent': ['error', 2],
    'vue/no-v-html': 'off',
    'max-len': ['error', {
      code: 150,
    }],
    'vue/max-attributes-per-line': ['error', {
      singleline: 2,
      multiline: {
        max: 1,
      },
    }],
    'no-underscore-dangle': ['error', { 
      allow: ['_id', '__typename']
    }],
    'no-unused-vars': ['error', { 'argsIgnorePattern': '^_' }],
    'template-curly-spacing' : 'off',
    indent : 'off',
  },

  parserOptions: {
    parser: 'babel-eslint',
  },
};
